package com.api.kingspark.parking.controller;


import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.reactivestreams.Publisher;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import com.api.kingspark.parking.domain.*;
import com.api.kingspark.parking.repositories.*;
import com.api.kingspark.parking.services.*;

import static org.mockito.ArgumentMatchers.any;

public class ParkingControllerTest {

    WebTestClient webTestClient;
    VehiculoRepository vehiculoRepository;
    ParkingController parkingController;
    PorteroServices porteroServices;
    TicketRepository ticketRepository;

    @Before
    public void setUp()throws Exception{

        vehiculoRepository= Mockito.mock(VehiculoRepository.class);
        ticketRepository= Mockito.mock(TicketRepository.class);
        porteroServices=Mockito.mock(PorteroServices.class);
        parkingController = new ParkingController(porteroServices, vehiculoRepository, ticketRepository);
        webTestClient= WebTestClient.bindToController(parkingController).build();


    }


    @Test
    public void list() {

        Vehicule car = new Vehicule();
        car.setLicense("RIP870");
        BDDMockito.given(vehiculoRepository.findAll()).willReturn(Flux.just(car));

        webTestClient.get().uri("/api/vehicules/").exchange().expectBodyList(Vehicule.class).hasSize(1);

    }

    @Test
    public void getById() {
        Vehicule car = new Vehicule();
        car.setLicense("RIP8700");
        BDDMockito.given(vehiculoRepository.findById("someid"))
                .willReturn(Mono.just(car));

        webTestClient.get()
                .uri("/api/vehicules/someid")
                .exchange()
                .expectBody(Vehicule.class);
    }

    @Test
    public void testcreateCarro(){
        Vehicule car = new Vehicule();
        //car.setLicense("TEST123");
        BDDMockito.given(vehiculoRepository.saveAll(any(Publisher.class))).willReturn(Flux.just(car));

       // Carro care = new Carro();
        car.setLicense("TEST123");
        Mono<Vehicule> caRToSaveMono = Mono.just(car);

        webTestClient.post()
                .uri("/api/vehicules")
                .body(caRToSaveMono, Vehicule.class)
                .exchange()
                .expectStatus()
                .isCreated();

    }

    @Test
    public void testUpdate(){

        Vehicule car = new Vehicule();
        //car.setLicense("TEST123");
        BDDMockito.given(vehiculoRepository.save(any(Vehicule.class))).willReturn(Mono.just(car));

        Vehicule care = new Vehicule();
        care.setLicense("TEST123");
        Mono<Vehicule> caRToUpdateMono = Mono.just(care);

        webTestClient.put().uri("/api/vehicules/update")
                .body(caRToUpdateMono,Vehicule.class)
                .exchange()
                .expectStatus()
                .isOk();

    }
}