package com.api.kingspark.parking.domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class VehiculeTest {

    Vehicule carro;

    @Before
    public void setUp() throws Exception {
        carro = new Vehicule();
    }

    @Test
    public void getId() {

        String idValue = "IDTEST";

        carro.setId(idValue);

        assertEquals(idValue, carro.getId());
    }

    @Test
    public void setId() {

        String idValue = "IDTESTT";

        carro.setId(idValue);

        assertEquals("IDTESTT", carro.getId());


    }

    @Test
    public void getParqueado() {
    }

    @Test
    public void setParqueado() {
    }

    @Test
    public void getPlaca() {

        String placa = "BUP960";
        carro.setLicense(placa);

        assertEquals("BUP960", carro.getLicense());
    }

    @Test
    public void setPlaca() {

        String placa = "BUP960";
        carro.setLicense(placa);

        assertEquals("BUP960", carro.getLicense());
    }

    @Test
    public void getCc() {

        Integer cc = 400;
        carro.setCc(cc);

        assertEquals(cc, carro.getCc());


    }

    @Test
    public void setCc() {
        Integer cc = 400;
        carro.setCc(cc);

        assertEquals(cc, carro.getCc());
    }

    @Test
    public void getTipo() {
        String tipo = "CARRO";
        carro.setTypeV(tipo);

        assertEquals(tipo, carro.getTypeV());
    }

    @Test
    public void testContruc(){
        Vehicule veh= new Vehicule("bup960",556,"CARRO");
        Integer cc=556;
        assertEquals("bup960",veh.getLicense());
        assertEquals(cc,veh.getCc());
        assertEquals("CARRO",veh.getTypeV());

    }

    @Test
    public void setTipo() {
        String tipo = "CARRO";
        carro.setTypeV(tipo);

        assertEquals(tipo, carro.getTypeV());
    }
}