package com.api.kingspark.parking.domain;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class TickectTest {

    Tickect tickect;
    Vehicule vehicule;

    @Before
    public void setUp() throws Exception {
        tickect = new Tickect();
        vehicule = new Vehicule();
    }

    @Test
    public void getVehiculo() {

        tickect.setVehicule(vehicule);


        assertEquals(vehicule, tickect.getVehicule());
    }

    @Test
    public void setVehiculo() {

        tickect.setVehicule(vehicule);


        assertEquals(vehicule, tickect.getVehicule());

    }

    @Test
    public void setIdVehiculo() {

        tickect.setIdVehicule("TEST");
        assertEquals("TEST", tickect.getIdVehicule());
    }

    @Test
    public void getIdVehiculo() {

        tickect.setIdVehicule("TEST");
        assertEquals("TEST", tickect.getIdVehicule());
    }

    @Test
    public void getFechaIn() {
       tickect.setDateIn("18-18-18");
        assertEquals("18-18-18", tickect.getDateIn());
    }

    @Test
    public void setFechaIn() {
        tickect.setDateIn("18-18-18");

        assertEquals("18-18-18", tickect.getDateIn());
    }

    @Test
    public void setFechaOUt() {
        tickect.setDateOut("18-18-18");

        assertEquals("18-18-18", tickect.getDateOut());
    }

    @Test
    public void getCobroTotal() {
        tickect.setBill(BigDecimal.TEN);

        assertEquals(BigDecimal.TEN,tickect.getBill());
    }

    @Test
    public void setCobroTotal() {
        tickect.setBill(BigDecimal.TEN);

        assertEquals(BigDecimal.TEN,tickect.getBill());
    }
}