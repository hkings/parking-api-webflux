package com.api.kingspark.parking.domain;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Calendar;

import static org.junit.Assert.*;

public class CalendarioDoormanTest {

    @Before
    public void setUp() throws Exception {

        CalendarioDoorman calendarioDoorman = Mockito.mock(CalendarioDoorman.class);

        Mockito.when(calendarioDoorman.getActualDay()).thenReturn(Calendar.THURSDAY);

    }

    @Test
    public void getActualDay() {

        CalendarioDoorman calendarioDoorman= new CalendarioDoorman();
        int dia=calendarioDoorman.getActualDay();

        assertEquals(Calendar.THURSDAY,dia);



    }
}