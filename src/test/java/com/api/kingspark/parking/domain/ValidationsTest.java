package com.api.kingspark.parking.domain;

import org.junit.Test;
import org.mockito.Mockito;

import java.util.Calendar;

import static org.junit.Assert.*;

public class ValidationsTest {

    @Test
    public void crearValidacionDiaNoHabilTest(){


        CalendarioDoorman calendarioDoorman = Mockito.mock(CalendarioDoorman.class);

        Mockito.when(calendarioDoorman.getActualDay()).thenReturn(Calendar.TUESDAY);


        ValidationsEntryPark validationsEntryPark = new Validations(calendarioDoorman);

        try {
            Vehicule vehicule = new Vehicule("ACC211", "MOTO");
            validationsEntryPark.esValidoPLacaDia(vehicule);
            //fail();

        }
        catch (Exception ex){
            assertEquals("La placa no puede ingresar que es un dia habil", ex.getMessage());
        }




    }

}
