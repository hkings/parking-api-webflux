package com.api.kingspark.parking.services;

import com.api.kingspark.parking.controller.ParkingController;
import com.api.kingspark.parking.domain.CalendarioDoorman;
import com.api.kingspark.parking.domain.Tickect;
import com.api.kingspark.parking.domain.ValidationsEntryPark;
import com.api.kingspark.parking.domain.Vehicule;
import com.api.kingspark.parking.exceptions.ExceptionPark;
import com.api.kingspark.parking.repositories.TicketRepository;
import com.api.kingspark.parking.repositories.VehiculoRepository;

import jdk.nashorn.internal.runtime.regexp.joni.Regex;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;

import static org.hamcrest.text.MatchesPattern.matchesPattern;
import static org.junit.Assert.*;


import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;

public class PorteroServicesImplTest {


    TicketRepository ticketRepository;


    VehiculoRepository vehiculoRepository;
    PorteroServices porteroServices;
    WebTestClient webTestClient;
    ParkingController parkingController;

    ValidationsEntryPark validacionIngresoParqueaderos;
    List<ValidationsEntryPark> validacionIngresoParqueaderosl;
    CalendarioDoorman calendarioDoorman;

    @Before
    public void setUp() throws Exception {

        ticketRepository= Mockito.mock(TicketRepository.class);
        calendarioDoorman=Mockito.mock(CalendarioDoorman.class);
        vehiculoRepository= Mockito.mock(VehiculoRepository.class);
        validacionIngresoParqueaderos= Mockito.mock(ValidationsEntryPark.class);
        porteroServices=Mockito.mock(PorteroServices.class);

        parkingController = new ParkingController(porteroServices,vehiculoRepository,ticketRepository);
        webTestClient= WebTestClient.bindToController(parkingController).build();
    }


    @Test
    public void guardarVehi(){
        Vehicule veh = new Vehicule("BUP960");



        porteroServices=new PorteroServicesImpl(ticketRepository,vehiculoRepository,validacionIngresoParqueaderosl);

        try {

            porteroServices.saveVehicule(veh);
            //Mockito.when(porteroServices.saveVehicule(veh)).thenThrow(new RuntimeException());
            //fail();

        }
        catch (ExceptionPark ex){
            assertEquals("no puede ingresar", ex.getMessage());
        }

    }

    @Test
    public void guardarVehifail(){

        Vehicule veh = new Vehicule("BUP960");
        porteroServices=new PorteroServicesImpl(ticketRepository,vehiculoRepository,validacionIngresoParqueaderosl);
        try {
            porteroServices.saveVehicule(veh);
            fail();
            }
        catch (ExceptionPark ex){
            assertEquals("no puede ingresar", ex.getMessage());
        }
        }



    @Test
    public void siNoTieneCcEsCarro(){

        Vehicule veh = new Vehicule();
        porteroServices = new PorteroServicesImpl(ticketRepository,vehiculoRepository,validacionIngresoParqueaderosl);

        porteroServices.isBike(veh);
        //veh =;
        //String tipo= veh.getTypeV();
        assertEquals("CARRO",veh.getTypeV());

    }

    @Test
    public void siTieneCcesMoto(){

        Vehicule veh = new Vehicule("BUP960",250);
        porteroServices = new PorteroServicesImpl(ticketRepository,vehiculoRepository,validacionIngresoParqueaderosl);

        porteroServices.isBike(veh);

        assertEquals("MOTO",veh.getTypeV());

    }

    @Test
    public void sacarCarrofail(){

        Vehicule veh = new Vehicule("BUP960");
        Tickect t1=new Tickect();
        t1.setDateIn("2018-02-13 00-04-50");
        t1.setVehicule(veh);

        given(vehiculoRepository.findById(veh.getId())).willReturn(Mono.just(veh));
        given(ticketRepository.findByidVehicule(veh.getId())).willReturn(Mono.just(t1));
        given(vehiculoRepository.delete(veh)).willReturn(Mono.empty());

        porteroServices=new PorteroServicesImpl(ticketRepository,vehiculoRepository,validacionIngresoParqueaderosl);


        try {

            porteroServices.drawVehicule(veh.getId());
            //fail();

        }
        catch (Exception ex){
            assertEquals("no puede salir", ex.getMessage());
        }


    }

    @Test
    public void sacarCarro(){

        Vehicule veh = new Vehicule("BUP960");
        Tickect t1=new Tickect();
        t1.setDateIn("2018-02-13 00-04-50");
        t1.setVehicule(veh);

        given(vehiculoRepository.findById(veh.getId())).willReturn(Mono.just(veh));
        given(ticketRepository.findByidVehicule(veh.getId())).willReturn(Mono.just(t1));
         given(vehiculoRepository.delete(veh)).willReturn(Mono.empty());

        porteroServices=new PorteroServicesImpl(ticketRepository,vehiculoRepository,validacionIngresoParqueaderosl);


        try {

            porteroServices.drawVehicule(veh.getId());
            //fail();

        }
        catch (Exception ex){
            assertEquals("no puede salir", ex.getMessage());
        }

    }

    @Test
    public void listarVehiculosParquados(){
        Tickect tickect= new Tickect();
        List<Tickect> listVeh ;

        given(ticketRepository.findByVehicule_inpark(1)).willReturn(Flux.just(tickect));

        porteroServices=new PorteroServicesImpl(ticketRepository,vehiculoRepository,validacionIngresoParqueaderosl);

      listVeh= porteroServices.getInVehicules();
      int size= listVeh.size();

      assertEquals(1,size);

    }

    @Test
    public void testcapacidadCarros(){

        Vehicule vehicule = new Vehicule();
        given(vehiculoRepository.findBytypeV("CARRO")).willReturn(Flux.just(vehicule));
        porteroServices=new PorteroServicesImpl(ticketRepository,vehiculoRepository,validacionIngresoParqueaderosl);
        try{
           porteroServices.capacityCar();
           //fail();
       }catch (ExceptionPark e){
           assertEquals("no hay capacidad para carros", e.getMessage());
       }
       }

    @Test
    public void testcapacidadMotos(){

        Vehicule vehicule = new Vehicule();
        given(vehiculoRepository.findBytypeV("MOTO")).willReturn(Flux.just(vehicule));
        porteroServices=new PorteroServicesImpl(ticketRepository,vehiculoRepository,validacionIngresoParqueaderosl);
        try{
            porteroServices.capacityBike();
            //fail();
        }catch (ExceptionPark e){
            assertEquals("no hay capacidad para motos", e.getMessage());
        }
    }

    @Test
    public void cobrar1DiaCarro(){
        porteroServices=new PorteroServicesImpl(ticketRepository,vehiculoRepository,validacionIngresoParqueaderosl);
        Long dia= Long.valueOf(86400);
        Vehicule vehicule = new Vehicule();

        BigDecimal valor=porteroServices.generateBill(dia, vehicule);
        assertEquals("8000",valor.toString());
    }

    @Test
    public void cobrar1HoraCarro(){
        porteroServices=new PorteroServicesImpl(ticketRepository,vehiculoRepository,validacionIngresoParqueaderosl);
        Long dia= Long.valueOf(3600);
        Vehicule vehicule = new Vehicule();

        BigDecimal valor=porteroServices.generateBill(dia, vehicule);
        assertEquals("1000",valor.toString());
    }

    @Test
    public void cobrar1horaMoto(){
        porteroServices=new PorteroServicesImpl(ticketRepository,vehiculoRepository,validacionIngresoParqueaderosl);
        Long dia= Long.valueOf(3600);
        Vehicule vehicule = new Vehicule("dada",84);

        BigDecimal valor=porteroServices.generateBill(dia, vehicule);
        assertEquals("500",valor.toString());
    }
    @Test
    public void cobrar1horaMotocc(){
        porteroServices=new PorteroServicesImpl(ticketRepository,vehiculoRepository,validacionIngresoParqueaderosl);
        Long dia= Long.valueOf(3600);
        Vehicule vehicule = new Vehicule("dada",500);

        BigDecimal valor=porteroServices.generateBill(dia, vehicule);
        assertEquals("2500",valor.toString());
    }

    @Test
    public void cobrar1DiaMoto(){
        porteroServices=new PorteroServicesImpl(ticketRepository,vehiculoRepository,validacionIngresoParqueaderosl);
        Long dia= Long.valueOf(86400);
        Vehicule vehicule = new Vehicule("dada",84);

        BigDecimal valor=porteroServices.generateBill(dia, vehicule);
        assertEquals("4000",valor.toString());
    }


    @Test
    public void generarFechaFormateada(){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh-mm-personas");
        Regex aaa= new Regex("yyyy-MM-dd hh-mm-personas");
        String patternToUse = sdf.toLocalizedPattern();


        porteroServices = new PorteroServicesImpl(ticketRepository,vehiculoRepository,validacionIngresoParqueaderosl);

        String fecha=porteroServices.dateFormat();
      //assertThat(fecha, matchesPattern(patternToUse));

       //assertThat(fecha,matchesPattern(a));

       // Assert.assertTrue(fecha.matches(sdf.toString()));
              //  assertTrue(Pattern.matches(patternToUse,fecha));
       // assertTh
        String aa="";
        try{
           aa= sdf.parse(fecha).toString();
        }catch (Exception e){

        }
        //Matcher as=a.matcher((fecha));
      // assertEquals(Matchers.matchesPattern(patternToUse),aa);
       // assertTrue(a.matcher(fecha));
       // assertTrue();
    }

/*
    @Test
    public void checkPlacaCarro() {
        //Arrange
        Vehicule car = new Vehicule();

        try {
            CalendarioDoorman calendarGuard = Mockito.mock(CalendarioDoorman.class);
            Mockito.when(calendarGuard.getActualDay()).thenReturn(Calendar.TUESDAY);

            //Act
            porteroServices = new PorteroServicesImpl(parqueaderoRepository,ticketRepository,vehiculoRepository);
            porteroServices.checkPlacaCarro(car);
           // fail();
        }catch (RuntimeException e){
            //Assert
            assertEquals("Vehicle cannot enter, license begin for A and today is not available day for it", e.getMessage());
        }
    }

    */
    }
