package com.api.kingspark.parking.controller;

import com.api.kingspark.parking.domain.Tickect;
import com.api.kingspark.parking.domain.Vehicule;
import com.api.kingspark.parking.repositories.TicketRepository;
import com.api.kingspark.parking.repositories.VehiculoRepository;
import com.api.kingspark.parking.services.PorteroServices;
import org.reactivestreams.Publisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

@RestController
public class ParkingController {


    private PorteroServices porteroServices;
    private final VehiculoRepository vehiculoRepository;
    private  final TicketRepository ticketRepository;


    public ParkingController(PorteroServices porteroServices, VehiculoRepository vehiculoRepository, TicketRepository ticketRepository) {

        this.porteroServices = porteroServices;
        this.vehiculoRepository = vehiculoRepository;
        this.ticketRepository = ticketRepository;
    }
    @CrossOrigin
    @GetMapping("/api/vehiculespark")
    List<Tickect> listIn(){
        return porteroServices.getInVehicules();
    }

    @GetMapping("/api/vehicules")
    Flux<Vehicule> listAll(){
        return vehiculoRepository.findAll();
    }

    @GetMapping("/api/vehicules/{id}")
    Mono<Vehicule> getById(@PathVariable String id){
        return vehiculoRepository.findById(id);
    }

    @CrossOrigin
    @PostMapping(value = "/api/vehicules",consumes = MediaType.APPLICATION_JSON_VALUE,produces={"application/json","application/xml"})
    @ResponseStatus(HttpStatus.CREATED)
    Mono<Void> createVehicule(@RequestBody Publisher<Vehicule> carstream){
        Vehicule ca= Mono.fromDirect(carstream).cache().block();
        return porteroServices.saveVehicule(ca);
        //return ResponseEntity.created(location).build();
    }

    @CrossOrigin
    @PatchMapping("/api/vehicules/{id}")
    @ResponseBody
    public ResponseEntity<Mono<Tickect>>update(@PathVariable String id){
        return ResponseEntity.ok(ticketRepository.save(porteroServices.drawVehicule(id))) ;
    }
}
