package com.api.kingspark.parking.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;



@Document
public class Tickect {

    //atributos

    @Id
    @DBRef
    private String id;

    private String idVehicule;

    private String dateIn;
    private String dateOut;
    private BigDecimal bill;
    private Vehicule vehicule;


    public Vehicule getVehicule() {
        return vehicule;
    }

    public void setVehicule(Vehicule vehicule) {
        this.vehicule = vehicule;
    }
    public void setIdVehicule(String idVehicule) {
        this.idVehicule = idVehicule;
    }

    public String getIdVehicule() {
        return idVehicule;
    }

    public String getDateIn() {
        return dateIn;
    }

    public String getDateOut() {
        return dateOut;
    }

    public void setDateIn(String dateIn) {
        this.dateIn = dateIn;
    }

    public void setDateOut(String dateOut) {
        this.dateOut = dateOut;
    }

    public BigDecimal getBill(){return bill;}

    public void setBill(BigDecimal bill) {
        this.bill = bill;
    }
}
