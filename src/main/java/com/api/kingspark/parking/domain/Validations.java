package com.api.kingspark.parking.domain;

import com.api.kingspark.parking.exceptions.ExceptionPark;
import org.springframework.stereotype.Component;

import java.util.Calendar;

@Component
public class Validations implements ValidationsEntryPark {


    private static final Character CARACTER_A = 'a';

    private static final int LUNES = Calendar.MONDAY;
    private static final int DOMINGO = Calendar.SUNDAY;

    private CalendarioDoorman calendarioDoorman;

    public Validations(CalendarioDoorman calendarioDoorman) {

        this.calendarioDoorman = calendarioDoorman;
    }

    private boolean esUnDiaHabil() {
        return (calendarioDoorman.getActualDay() == LUNES || calendarioDoorman.getActualDay() == DOMINGO);
    }

    @Override
    public void esValido(Vehicule vehicule) {
        esValidoPLacaDia(vehicule);
    }

    @Override
    public void esValidoPLacaDia(Vehicule vehicule) {

        String placa = vehicule.getLicense().toLowerCase();
        Character primerCaracterPlaca = placa.charAt(0);

        if (primerCaracterPlaca == CARACTER_A && !esUnDiaHabil()) {

            throw new ExceptionPark("La placa no puede ingresar que es un dia habil");

        }
    }
}
