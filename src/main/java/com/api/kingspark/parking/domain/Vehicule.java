package com.api.kingspark.parking.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;


@Document
public  class Vehicule {

    //atributos
    @Id
    @DBRef
    private  String id= UUID.randomUUID().toString();
    private String license;
    private Integer cc;
    private String typeV;
    private Integer inpark;

    //Contructores
    public Vehicule(String license, Integer cc, String typeV) {
        this.license = license;

        this.cc = cc;
        this.typeV = typeV;
    }

    public Vehicule(String license, String typeV) {
        this.license = license;

        this.typeV = typeV;
    }

    public Vehicule(String license, Integer cc){
        this.license = license;
        this.cc = cc;

    }

    public Vehicule(String license){

    }

    public Vehicule(){}


    //Getters And Setters


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getInpark() {
        return inpark;
    }

    public void setInpark(Integer inpark) {
        this.inpark = inpark;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }



    public Integer getCc() {
        return cc;
    }

    public void setCc(Integer cc) {
        this.cc = cc;
    }

    public String getTypeV() {
        return typeV;
    }

    public void setTypeV(String typeV) {
        this.typeV = typeV;
    }




}
