package com.api.kingspark.parking.repositories;

import com.api.kingspark.parking.domain.Vehicule;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

public interface VehiculoRepository extends ReactiveMongoRepository<Vehicule,String> {

    Flux<Vehicule> findBytypeV(String tipo);


}
