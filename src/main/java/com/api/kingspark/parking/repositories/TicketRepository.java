package com.api.kingspark.parking.repositories;

import com.api.kingspark.parking.domain.Tickect;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TicketRepository extends ReactiveMongoRepository<Tickect,String> {

    Mono<Tickect> findByidVehicule(String id);

    Flux<Tickect> findByVehicule_inpark(Integer isIn);



}
