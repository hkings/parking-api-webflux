package com.api.kingspark.parking;

import com.api.kingspark.parking.repositories.TicketRepository;
import com.api.kingspark.parking.repositories.VehiculoRepository;
import com.api.kingspark.parking.services.PorteroServices;
import com.api.kingspark.parking.services.PorteroServicesImpl;
import com.api.kingspark.parking.domain.CalendarioDoorman;
import com.api.kingspark.parking.domain.Validations;
import com.api.kingspark.parking.domain.ValidationsEntryPark;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class ApplicationConfiguration {

    @Bean
    @Primary
    //@Autowired(required = true)
    public PorteroServices createPortero(TicketRepository ticketRepository,VehiculoRepository vehiculoRepository){


        List<ValidationsEntryPark> validationsEntryParks = new ArrayList<>();

        validationsEntryParks.add(new Validations(new CalendarioDoorman()));

        return new PorteroServicesImpl(ticketRepository,vehiculoRepository, validationsEntryParks);
    }


}
