package com.api.kingspark.parking.services;

import com.api.kingspark.parking.domain.Tickect;
import com.api.kingspark.parking.domain.ValidationsEntryPark;
import com.api.kingspark.parking.domain.Vehicule;
import com.api.kingspark.parking.exceptions.ExceptionPark;
import com.api.kingspark.parking.repositories.*;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;


import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;


@Service
public class PorteroServicesImpl implements PorteroServices {

    public static final String MOTO = "MOTO";
    public static final String CARRO = "CARRO";
    private static final int MAX_CARROS=19;
    private static final int MAX_MOTOS=9;
    public static final int HOURS_TO_MAKE_DAY = 9;

    TicketRepository ticketRepository;
    VehiculoRepository vehiculoRepository;
    private static final BigDecimal HORACARROM9=new BigDecimal(1000);
    private static final BigDecimal DIACARROM9=new BigDecimal(8000);
    private static final BigDecimal HORAMOTOM9=new BigDecimal(500);
    private static final BigDecimal DIAMOTOM9=new BigDecimal(4000);
    private static final BigDecimal CILINDRAJE=new BigDecimal(2000);
    private static final BigDecimal DIA=new BigDecimal(24);
    private static final BigDecimal LIMIT=new BigDecimal(9);
     SimpleDateFormat sIMPLEFORMATEER =new SimpleDateFormat("EEE MMM dd HH:mm:personas zzz yyyy");
     SimpleDateFormat fORMATTER = new SimpleDateFormat("yyyy-MM-dd hh-mm-personas");
    List<ValidationsEntryPark> validationsEntryParks;
    Logger logger = Logger.getLogger(PorteroServicesImpl.class.getName());


    public PorteroServicesImpl(TicketRepository ticketRepository, VehiculoRepository vehiculoRepository,List<ValidationsEntryPark> validationsEntryParks) {

        this.ticketRepository = ticketRepository;
        this.vehiculoRepository = vehiculoRepository;
        this.validationsEntryParks = validationsEntryParks;
    }

    @Override
    public Mono<Void> saveVehicule(Vehicule vehicule) {

        Tickect tickect= new Tickect();
        isBike(vehicule);
        vehicule.setInpark(1);
        tickect.setIdVehicule(vehicule.getId());
        String formattedDate = dateFormat();
        tickect.setDateIn(formattedDate);
        tickect.setVehicule(vehicule);
        try{
            this.validationsEntryParks.stream().forEach(validacion -> validacion.esValidoPLacaDia(vehicule));
            checkCapacityVehicules(vehicule);
        }catch (Exception ex){
            if(ex.getMessage()=="La placa no puede ingresar que es un dia habil"){
                throw new ExceptionPark("no puede ingresar por dia invalido");
            }
            if(ex.getMessage()=="no hay capacidad para carros"){
                throw new ExceptionPark("no puede ingresar por capacidad Carros");
            }
            if(ex.getMessage()=="no hay capacidad para motos"){
                throw new ExceptionPark("no puede ingresar por capacidad Motos");
            }

           // throw new ExceptionPark("no puede ingresar");
        }
        vehiculoRepository.save(isBike(vehicule)).block();
        return ticketRepository.save(tickect).then();
    }

    public void checkCapacityVehicules(Vehicule vehicule){
        if(vehicule.getTypeV() == MOTO){
            capacityBike();
        }else{
            capacityCar();
        }
    }

    public Vehicule isBike(Vehicule vehicule){

        if(vehicule.getCc()!=null){
        vehicule.setTypeV(MOTO);
        return vehicule;
        }else {
            vehicule.setTypeV(CARRO);
            return vehicule;
        }

    }

    public String dateFormat() {
        String in = new Date().toString();

        Date date;

        String formattedDate=null;

        try{
             date = sIMPLEFORMATEER.parse(in);
             formattedDate = fORMATTER.format(date);
        }
        catch (Exception e){
            logger.warning(e.getMessage());
        }
        return formattedDate;
    }

    @Override
    public void capacityCar() {

        List<Vehicule> listavehi= new ArrayList<>();
        vehiculoRepository.findBytypeV(CARRO).toIterable().forEach(listavehi::add);

        Integer tam = listavehi.size();
        if(tam>MAX_CARROS){
            throw new ExceptionPark("no hay capacidad para carros");
        }

    }

    @Override
    public List<Tickect> getInVehicules() {
        List<Tickect> listVehIn= new ArrayList<>();
        ticketRepository.findByVehicule_inpark(1).toIterable().forEach(listVehIn::add);

        return listVehIn;
    }

    @Override
    public void capacityBike() {

        List<Vehicule> listavehi= new ArrayList<>();
        vehiculoRepository.findBytypeV(MOTO).toIterable().forEach(listavehi::add);


        Integer tam = listavehi.size();
        if(tam>MAX_MOTOS){
            throw new ExceptionPark("no hay capacidad para motos");
        }

    }

    public List<BigDecimal> typeOfBill(BigDecimal value,Integer cc){


        List<BigDecimal> listOfBillCaracteristics= new ArrayList<>();
        if(cc==null){
            BigDecimal valorHora=HORACARROM9;
            listOfBillCaracteristics.add(valorHora);
            BigDecimal valorDia=DIACARROM9;
            listOfBillCaracteristics.add(valorDia);

        }else{
            BigDecimal valorHora=HORAMOTOM9;
            listOfBillCaracteristics.add(valorHora);
            BigDecimal valorDia=DIAMOTOM9;
            listOfBillCaracteristics.add(valorDia);
            if(cc>= 500){
                value=value.add(CILINDRAJE);
                listOfBillCaracteristics.add(value);
            }else{ listOfBillCaracteristics.add(value);}
        }

        return listOfBillCaracteristics;
    }

    public BigDecimal generateBill(Long timeFin, Vehicule vehicule) {

        Integer cilindraje= vehicule.getCc();
        BigDecimal tiempo =BigDecimal.valueOf(timeFin.doubleValue()/3600);
        long iPart = tiempo.longValue();
        BigDecimal fraccion = tiempo.remainder(BigDecimal.ONE);
        BigDecimal valor=BigDecimal.ZERO;
        List<BigDecimal> ll = typeOfBill(valor,cilindraje);
        BigDecimal valorHoraACobrar=ll.get(0);
        BigDecimal valorDiaACobrar=ll.get(1);
        if(cilindraje!=null){valor=ll.get(2);}
        int selector = getSelector(iPart);
        valor = calculateValue(iPart, fraccion, valor, valorHoraACobrar, valorDiaACobrar, selector);
        return valor;

    }

    private BigDecimal calculateValue(long iPart, BigDecimal fraccion, BigDecimal valor, BigDecimal valorHoraACobrar, BigDecimal valorDiaACobrar, int selector) {
        switch (selector){
            case 1:
                valor=valor.add(valorHoraACobrar.multiply(BigDecimal.valueOf(iPart))) ;
                if(fraccion.compareTo(BigDecimal.ZERO) > 0){
                    valor=valor.add(valorHoraACobrar);
                }
                break;
            case 2:
                valor=valorDiaACobrar;
                break;
            case 3:
                Long diaa=iPart/24;
                BigDecimal dias=BigDecimal.valueOf(iPart).divide(DIA, MathContext.DECIMAL128);
                BigDecimal extra = dias.remainder(BigDecimal.ONE,MathContext.DECIMAL128).multiply(DIA).add(fraccion);
                valor=valor.add(BigDecimal.valueOf(diaa).multiply(valorDiaACobrar));

                if(extra.setScale(0, RoundingMode.UP).compareTo(LIMIT) >= 0){
                    valor=valor.add(valorDiaACobrar);
                }else{
                    valor=valor.add( extra.setScale(0, RoundingMode.UP).multiply(valorHoraACobrar));
                }
                break;
                default:
                    break;
        }
        return valor;
    }

    private int getSelector(long iPart) {
        int selector;
        if (iPart < HOURS_TO_MAKE_DAY) {
            selector=1;
        }else if(iPart> HOURS_TO_MAKE_DAY &&iPart<DIA.longValue()){
            selector=2;
        }else{
            selector=3;
        }
        return selector;
    }

    private List<Long> formatTimeInAndOut(String dateIn,String dateOut){

            List<Long> times = new ArrayList<>();
        try{
            Date dateOutt = fORMATTER.parse(dateOut);
            Date dateInn = fORMATTER.parse(dateIn);
           Long timeOut= dateOutt.getTime();
           times.add(timeOut);
           Long timeIn= dateInn.getTime();
            times.add(timeIn);
            return times;
        }
        catch (Exception e){
            logger.warning(e.getMessage());
        }

        return times;
    }

    @Override
    public Tickect drawVehicule(String id) {

            if(id!=null){
                String idd = id;
                Vehicule vehicule1 = vehiculoRepository.findById(idd).block();
                Mono<Tickect> ticket= ticketRepository.findByidVehicule(idd);
                Tickect tickectt=ticket.block();
                String formattedDate = dateFormat();
                tickectt.setDateOut(formattedDate);
                String dateeIn=tickectt.getDateIn();
                List<Long> times = formatTimeInAndOut(dateeIn,formattedDate);
                Long timeOut=times.get(0);
                Long timeIn=times.get(1);
                Long timeFinal = (timeOut-timeIn)/1000;
                BigDecimal valor = generateBill(timeFinal,vehicule1);
                tickectt.getVehicule().setInpark(0);
                tickectt.setBill(valor);
                vehiculoRepository.delete(vehicule1).block();
                return tickectt;
            }else {
                throw new ExceptionPark("no puede salir");
            }
    }
}
