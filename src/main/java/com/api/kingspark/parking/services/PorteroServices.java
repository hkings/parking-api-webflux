package com.api.kingspark.parking.services;

import com.api.kingspark.parking.domain.Tickect;
import com.api.kingspark.parking.domain.Vehicule;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.List;

public interface PorteroServices {

    Mono<Void> saveVehicule(Vehicule vehicule);

    List<Tickect> getInVehicules();

    void capacityCar();

    void capacityBike();

    void checkCapacityVehicules(Vehicule vehicule);

    Tickect drawVehicule(String id);

    Vehicule isBike(Vehicule vehicule);

    BigDecimal generateBill(Long d, Vehicule vehicule);

    String dateFormat();

}
